package ru.eliseev.teletask.domain.dto;

public class TaskCreateDto {
    private final Integer userId;
    private final String title;
    private final String description;

    public TaskCreateDto(Integer userId, String title, String description) {
        if (userId == null) {
            throw new IllegalArgumentException("userId can not be null");
        }
        if (title == null) {
            title = "";
        }
        if (description == null) {
            description = "";
        }
        this.userId = userId;
        this.title = title;
        this.description = description;
    }

    public Integer getUserId() {
        return userId;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }
}
