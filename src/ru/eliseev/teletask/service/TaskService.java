package ru.eliseev.teletask.service;

import ru.eliseev.teletask.domain.dto.TaskCreateDto;

public interface TaskService {
    void create(TaskCreateDto taskCreateDto);

    void showAll();
}
