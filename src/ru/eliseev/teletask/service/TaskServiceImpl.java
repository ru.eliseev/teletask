package ru.eliseev.teletask.service;

import ru.eliseev.teletask.domain.Task;
import ru.eliseev.teletask.domain.dto.TaskCreateDto;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class TaskServiceImpl implements TaskService {

    private Map<Long, Task> taskStorage;

    private final AtomicLong taskCount = new AtomicLong(0);

    @Override
    public void create(TaskCreateDto taskCreateDto) {
        if (taskStorage == null) {
            taskStorage = new HashMap<>();
        }
        long index = taskCount.incrementAndGet();
        Task task = new Task(index, taskCreateDto.getUserId(), taskCreateDto.getTitle(), taskCreateDto.getDescription());
        taskStorage.put(index, task);
    }

    @Override
    public void showAll() {
        System.out.println(taskStorage);
    }
}
