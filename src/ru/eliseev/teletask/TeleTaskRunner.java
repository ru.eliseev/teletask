package ru.eliseev.teletask;

import ru.eliseev.teletask.domain.dto.TaskCreateDto;
import ru.eliseev.teletask.service.TaskService;
import ru.eliseev.teletask.service.TaskServiceImpl;

public class TeleTaskRunner {
    public static void main(String[] args) {
        TaskService taskService = new TaskServiceImpl();
        for (int i = 1; i <= 100; i++) {
            taskService.create(new TaskCreateDto(1, "Task" + i, "Description" + i));
        }
        taskService.showAll();
    }
}